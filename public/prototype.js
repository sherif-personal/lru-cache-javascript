// Implementing LRU using prototypes instead of Classes
function LRUCachePrototype(capacity, item = {}) {

    if (typeof capacity == 'undefined' || capacity == null) {
        throw 'Cache capacity is not defined';
    }

    let capacityValue = capacity;

    Object.defineProperty(this, 'capacity', {
        get() {
            return capacityValue;
        },
        set(newValue) {
            capacityValue = newValue
            this.checkCapacity();
        },
        enumerable: false,
        configurable: true
    });

    Object.defineProperty(this, 'size', {
        get() { return Object.keys(this).length }
    });

    Object.defineProperty(this, 'cacheOrder', {
        value: [],
        writable: true,
        enumerable: false
    });

    if (item) {
        this.cache(Object.keys(item)[0], Object.values(item)[0])
        this.setCacheOrder(Object.keys(item)[0])
    }

}

LRUCachePrototype.prototype.cache = function (key, value) {

    // improvement to check on types for key and value
    if (typeof key == 'undefined' || key == null) {
        throw 'Cache must include a key';
    }

    if (typeof value == 'undefined' || value == null) {
        throw 'Cache must include a value';
    }

    let cacheItemValue = value;

    Object.defineProperty(this, key, {
        get() {
            this.setCacheOrder(key);
            return cacheItemValue;
        },
        set(newValue) {
            cacheItemValue = newValue
        },
        enumerable: true,
        configurable: true
    })

    this.setCacheOrder(key);
    this.checkCapacity();

    return this

}

LRUCachePrototype.prototype.setCacheOrder = function (key) {
    let cacheIndex = this.cacheOrder.indexOf(key);

    if (cacheIndex !== -1) {
        this.cacheOrder.unshift(this.cacheOrder.splice(cacheIndex, 1)[0]);
    } else {
        this.cacheOrder.unshift(key)
    }
}

LRUCachePrototype.prototype.deleteCacheItem = function (key) {
    delete this[key];
    this.cacheOrder = this.cacheOrder.filter(item => item !== key);
}

LRUCachePrototype.prototype.checkCapacity = function () {
    if (this.size > this.capacity) {
        this.deleteCacheItem(this.cacheOrder[this.cacheOrder.length - 1])
    }
}



try {

    console.log("////////////////////////////////////////////")
    console.log("/////// LRU Prototype Implementation ///////")
    const storePrototype = new LRUCachePrototype(3, { a: 7 });
    console.log(storePrototype.size)
    console.log(storePrototype.capacity)
    console.log(storePrototype.a)
    console.log(storePrototype.cache('b', 2)['b'])

    storePrototype.a = 5;
    console.log(storePrototype.a)
    console.log(storePrototype.cache('c', 12).cache('d', 4)['b'])

    storePrototype.deleteCacheItem('d');
    console.log(storePrototype.d)
    console.log(storePrototype.size)

    storePrototype.cache('c', 6);
    console.log(storePrototype.c);

    storePrototype.capacity = 1;
    console.log(storePrototype.capacity);
    console.log(storePrototype.size)

    console.log(storePrototype.c);
    console.log(storePrototype.a)

    console.log(storePrototype)


} catch (error) {
    console.log("error: ", error)
}
