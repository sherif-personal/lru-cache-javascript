class LRUCache {
    constructor(capacity, item = {}) {

        if (typeof capacity == 'undefined' || capacity == null) {
            throw 'Cache capacity is not defined';
        }

        this.cacheCapacity = capacity;
        this.cacheList = item;

        // improvement: storing the time stamp in the item as a property
        this.lruOrder = [...Object.keys(item)];
    }

    get capacity () {
        return this.cacheCapacity;
    }

    set capacity (value) {

        //Before setting the capacity check capacity < size
        if (value < this.size) {
            for (let i = this.lruOrder.length - 1; i >= value; i--) {
                this.delete(this.lruOrder[i])
            }
        }

        this.cacheCapacity = value;
    }

    get size () {
        return this.cacheList ? Object.keys(this.cacheList).length : 0;
    }

    get cacheItems () {
        return this.cacheList;
    }

    getItem (key) {
        if (this.cacheList[key]) {

            // Update the order of the LRU array
            this.updateLruOrder(key);

            return this.cacheList[key]
        } else {
            return undefined;
        }
    }

    cache (key, value) {

        // improvement to check on types for key and value
        if (typeof key == 'undefined' || key == null) {
            throw 'Cache must include a key';
        }

        if (typeof value == 'undefined' || value == null) {
            throw 'Cache must include a value';
        }

        // Add item to the cache list object
        this.cacheList[key] = value;

        // Update the order of the LRU array
        this.updateLruOrder(key);

        // Check if the capacity equals size after adding the new item remove last item from cache and LruOrder
        if (this.size > this.capacity) {
            this.delete(this.lruOrder[this.lruOrder.length - 1]);
        }

        return this
    }

    delete (key) {
        delete this.cacheList[key]
        this.lruOrder = this.lruOrder.filter(item => item !== key);
    }

    updateLruOrder (key) {

        if (this.lruOrder.length == 0) {
            this.lruOrder.push(key)
        } else {

            //If key already exists move it to the front
            let cacheIndex = this.lruOrder.indexOf(key);

            if (cacheIndex !== -1) {
                this.lruOrder.unshift(this.lruOrder.splice(cacheIndex, 1)[0]);
            } else {
                this.lruOrder.unshift(key)
            }
        }
    }

    get lruList () {
        return this.lruOrder;
    }
}


 try {

    console.log("////////////////////////////////////////////")
    console.log("/////// LRU Class Implementation ///////")
    const store = new LRUCache(3, { a: 7 });

    console.log(store.size);
    console.log(store.capacity);
    console.log(store.getItem('a'));
    console.log(store.cache('b', 2).getItem('b'));

    store.cache('a',5);
    console.log(store.getItem('a'));
    console.log(store.cache('c', 12).cache('d', 4).getItem('b'));

    store.delete('d');
    console.log(store.getItem('d'));
    console.log(store.size);

    store.cache('c', 6);
    console.log(store.getItem('c'));

    store.capacity = 1;
    console.log(store.capacity);
    console.log(store.size);

    console.log(store.getItem('c'));
    console.log(store.getItem('a'));


} catch (error) {
    console.log("error: ", error);
}


